###############################################################################
# plotting functions for data visualization
class plot:
    ###########################################################################
    # Type-A figure: general plot     
    def typeA(df, save_name):
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(311)
        df['accel_x'].plot(linestyle='-', linewidth=0.5, color='r', grid=True)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['x-component'], fontsize=6)
        ax.set_ylabel('Accel. Obs.', fontsize=8)
        ax.tick_params(axis='both', which='both', labelsize=6)
        ax.set_title('Accelerometer Data - Regular Plot', fontsize=10)
        
        ax = fig.add_subplot(312)
        df['accel_y'].plot(linestyle='-', linewidth=0.5, color='b', grid=True)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['y-component'], fontsize=6)
        ax.set_ylabel('Accel. Obs.', fontsize=8)
        ax.tick_params(axis='both', which='both', labelsize=6)
        
        ax = fig.add_subplot(313)
        df['accel_z'].plot(linestyle='-', linewidth=0.5, color='k', grid=True)
        ax_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['z-component'], fontsize=6)
        ax.set_ylabel('Accel. Obs.', fontsize=8)
        ax.set_xlabel('Time', fontsize=10)
        ax.tick_params(axis='both', which='both', labelsize=6)
        
        #plt.show()
        fig.savefig(save_name+'.jpg', dpi=600, bbox_inches = "tight")
    
    ###########################################################################
    # Type-B figure: Positive and Negative avlues are plotted with different colors 
    def typeB(df, save_name):    
        import matplotlib.pyplot as plt
        import numpy as np
        fig = plt.figure()
        ax = fig.add_subplot(311)
        df.accel_x.where(df.accel_x.ge(0), np.nan).plot(linestyle='-', 
                        linewidth=0.5, color='green')
        df.accel_x.where(df.accel_x.lt(0), np.nan).plot(linestyle='-', 
                        linewidth=0.5, color='orange')
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.set_ylabel('Accel. Obs.', fontsize=8)
        ax.legend(['x-component'], fontsize=6)
        ax.tick_params(axis='both', which='both', labelsize=6)
        ax.set_title('Accelerometer Data - Positive/Negative Plot', fontsize=10)
        
        ax = fig.add_subplot(312)
        df.accel_y.where(df.accel_y.ge(0), np.nan).plot(linestyle='-', 
                        linewidth=0.5, color='green')
        df.accel_y.where(df.accel_y.lt(0), np.nan).plot(linestyle='-', 
                        linewidth=0.5, color='orange')
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['y-component'], fontsize=6)
        ax.set_ylabel('Accel. Obs.', fontsize=8)
        ax.tick_params(axis='both', which='both', labelsize=6)
        
        ax = fig.add_subplot(313)
        df.accel_z.where(df.accel_z.ge(0), np.nan).plot(linestyle='-', 
                        linewidth=0.5, color='green')
        df.accel_z.where(df.accel_z.lt(0), np.nan).plot(linestyle='-', 
                        linewidth=0.5, color='orange')
        ax_axis = ax.axes.get_xaxis()
        ax_axis.set_visible(True)
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['z-component'], fontsize=6)
        ax.set_ylabel('Accel. Obs.', fontsize=8)
        ax.set_xlabel('Time', fontsize=10)
        ax.tick_params(axis='both', which='both', labelsize=6)
        
        #plt.show()
        fig.savefig(save_name+'.jpg', dpi=600, bbox_inches = "tight")

    ###########################################################################
    # Zoom-in plot for selected time frame
    def motion(df, start, end, save_name):
        import matplotlib.pyplot as plt
        start_time = datetime(start[0],start[1],start[2],start[3],start[4])
        end_time   = datetime(end[0],end[1],end[2],end[3],end[4])
        
        df_new = df[(df.index >= start_time) & (df.index <= end_time)]
    
        fig = plt.figure()   
        fig.subplots_adjust(hspace=.5)
        
        # plot particle motion
        ax = fig.add_subplot(341)       
        ax = sns.scatterplot(x='accel_x', y='accel_y', data=df_new)   
        plt.axhline(y=0, color='r', linestyle='--') # add zero lines
        plt.axvline(x=0, color='r', linestyle='--')   
        ax.set_xlim([-1100, 1100])
        ax.set_ylim([-1100, 1100])
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_xlabel('X-comp.', fontsize=10)
        ax.set_ylabel('Y-comp.', fontsize=10)
        plt.text(850, 750, 'I', fontsize=8, color='r')
        plt.text(-1000, 750, 'II', fontsize=8, color='r')
        plt.text(-1000, -950, 'III', fontsize=8, color='r')
        plt.text(800, -950, 'IV', fontsize=8, color='r')

        ax = fig.add_subplot(345)       
        ax = sns.scatterplot(x='accel_x', y='accel_z', data=df_new)   
        plt.axhline(y=0, color='r', linestyle='--') # add zero lines
        plt.axvline(x=0, color='r', linestyle='--')   
        ax.set_xlim([-1100, 1100])
        ax.set_ylim([-1100, 1100])
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_xlabel('X-comp.', fontsize=10)
        ax.set_ylabel('Z-comp.', fontsize=10)
        plt.text(850, 750, 'I', fontsize=8, color='r')
        plt.text(-1000, 750, 'II', fontsize=8, color='r')
        plt.text(-1000, -950, 'III', fontsize=8, color='r')
        plt.text(800, -950, 'IV', fontsize=8, color='r')

        ax = fig.add_subplot(349)       
        ax = sns.scatterplot(x='accel_y', y='accel_z', data=df_new)   
        plt.axhline(y=0, color='r', linestyle='--') # add zero lines
        plt.axvline(x=0, color='r', linestyle='--')   
        ax.set_xlim([-1100, 1100])
        ax.set_ylim([-1100, 1100])
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_ylabel('Accel. Obs.', fontsize=8)
        ax.set_xlabel('Y-comp.', fontsize=10)
        ax.set_ylabel('Z-comp.', fontsize=10)
        plt.text(850, 750, 'I', fontsize=8, color='r')
        plt.text(-1000, 750, 'II', fontsize=8, color='r')
        plt.text(-1000, -950, 'III', fontsize=8, color='r')
        plt.text(800, -950, 'IV', fontsize=8, color='r')
        
        # plot zoom-in time series 
        ax = fig.add_subplot(342)
        df_new.accel_x.plot(linestyle='-', linewidth=0.5, color='r', grid=True)
        ax.legend(['x-comp.'], fontsize=6)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.set_ylim([-1100, 1100])
        ax.set_title(save_name)
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_yticklabels([])
        
        ax = fig.add_subplot(346)
        df_new.accel_y.plot(linestyle='-', linewidth=0.5, color='b', grid=True)
        ax.legend(['y-comp.'], fontsize=6)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.set_ylim([-1100, 1100])
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_yticklabels([])
        
        ax = fig.add_subplot(3,4,10)
        df_new.accel_z.plot(linestyle='-', linewidth=0.5, color='k', grid=True)
        ax.legend(['z-comp.'], fontsize=6)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.set_ylim([-1100, 1100])  
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_yticklabels([])
        
        # plot range
        ax = fig.add_subplot(322)
        df['accel_x'].plot(linestyle='-', linewidth=0.2, color='r', grid=True)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['x-comp.'], fontsize=6)
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_xlabel('')
        ymin, ymax = ax.get_ylim()
        ax.vlines(x=start_time, ymin=ymin, ymax=ymax-1, color='orange', linewidth=0.5)
        ax.vlines(x=end_time, ymin=ymin, ymax=ymax-1, color='orange', linewidth=0.5)
        ax.hlines(y=ymax, xmin=start_time, xmax=end_time, color='orange', linewidth=0.5)
        ax.hlines(y=ymin, xmin=start_time, xmax=end_time, color='orange', linewidth=0.5)
        
        ax = fig.add_subplot(324)
        df['accel_y'].plot(linestyle='-', linewidth=0.2, color='b', grid=True)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['y-comp.'], fontsize=6)
        ax.tick_params(axis='both', which='both', labelsize=4)
        ax.set_xlabel('')
        ymin, ymax = ax.get_ylim()
        ax.vlines(x=start_time, ymin=ymin, ymax=ymax-1, color='orange', linewidth=0.5)
        ax.vlines(x=end_time, ymin=ymin, ymax=ymax-1, color='orange', linewidth=0.5)
        ax.hlines(y=ymax, xmin=start_time, xmax=end_time, color='orange', linewidth=0.5)
        ax.hlines(y=ymin, xmin=start_time, xmax=end_time, color='orange', linewidth=0.5)
        
        ax = fig.add_subplot(326)
        df['accel_z'].plot(linestyle='-', linewidth=0.2, color='k', grid=True)
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(True)
        ax.grid(True, which='minor', axis='both')
        ax.grid(which='major', axis='both', linestyle='--')
        ax.legend(['z-comp.'], fontsize=6)
        ax.tick_params(axis='both', which='both', labelsize=4)        
        ymin, ymax = ax.get_ylim()
        ax.vlines(x=start_time, ymin=ymin, ymax=ymax-1, color='orange', linewidth=0.5)
        ax.vlines(x=end_time, ymin=ymin, ymax=ymax-1, color='orange', linewidth=0.5)
        ax.hlines(y=ymax, xmin=start_time, xmax=end_time, color='orange', linewidth=0.5)
        ax.hlines(y=ymin, xmin=start_time, xmax=end_time, color='orange', linewidth=0.5)
        
        # save to file
        # plt.show()
        fig.savefig(save_name+'.jpg', dpi=600, bbox_inches = "tight")

###############################################################################     