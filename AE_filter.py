###############################################################################
class filter:
###############################################################################
    def medfilt(df):
        from scipy import signal
        df2['accel_x'] = signal.medfilt(df['accel_x'])
        df2['accel_y'] = signal.medfilt(df['accel_y'])
        df2['accel_z'] = signal.medfilt(df['accel_z'])
        return df2

    # function to load Accelerometer data to Pandas DataFrame format
#    def highpass(df):
 
###############################################################################
    def FFTfilter(data, type, lowcut, highcut, fs, order=4):
    # bp = FFTfilter(data, 'band', 2,   8, 20, order=4)
    # hp = FFTfilter(data, 'high', 5,   0, 20, order=4)
    # lp = FFTfilter(data,  'low', 0, 0.2, 20, order=4)    
        from scipy.signal import butter, lfilter, tukey
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        if type=='band':
            b, a = butter(order, [low, high], btype='band')
        elif type=='high':
            b, a = butter(order, [low], btype='high')
        elif type=='low':
            b, a = butter(order, [high], btype='low')
        
        y = lfilter(b, a, data)

        # Apply Tukey (tapered cosine) window
        # apply 25% of taper on both ends
        # https://docs.scipy.org/doc/scipy-0.16.0/reference/generated/scipy.signal.tukey.html
        # https://www.programcreek.com/python/example/100542/scipy.signal.tukey
        taper = tukey(len(data), alpha=0.25)
        output = y*taper
        return output
    # ref: https://stackoverflow.com/questions/54635201/filter-on-multiple-columns-in-pandas-data-frame-using-a-loop

############################################################################### 