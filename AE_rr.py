###############################################################################
class rr:
###############################################################################
    def LMMSE(self, y, var_RR):
        from statistics import variance, mean
        # ref: Gharibans et al., Scientific Reports, 2018
        # y = df_cut.accel_z.values

        E_y = mean(y)       # local mean E(y)

        var_y = variance(y) # variance
        # var_RR = 36 # long-term average # from '2017-06-16 01:15:00 - 01:27:00'
        # var_RR = 80
	     
        if (var_y > var_RR):
            x_artifact = E_y + ((var_y-var_RR)/var_y)*(y-E_y)
        elif  (var_y <= var_RR):
            x_artifact = E_y

        rr = y - x_artifact
        return rr
	     
###############################################################################
    def rr_estimation(self, df_cut, cmp):
        import numpy as np 
        from scipy import signal
        from scipy.signal import find_peaks

        win = 1 * 60 * 50 # cut every 1-minute

        # apply LMMSE:
        new = np.array([])
        var_RR = 80

        for w in range(0,(round(len(df_cut[cmp])/win)-1)):
            if w<(round(len(df_cut[cmp])/win)-1-1):
                t1 = win * (w)
                t2 = t1 + win
                y = df_cut[cmp].values[t1:t2]
                rr = self.LMMSE(y, var_RR)
            else:
                t1 = win * (w)
                t2 = t1 + win
                y = df_cut[cmp].values[t1:]
                rr = self.LMMSE(y, var_RR) 
        new = np.concatenate([new, rr])

        # find peaks
        data = new
        f, Pxx_den = signal.welch(data, 50, nperseg=4096)

        f_n = f[np.where((f>=0.17) & (f<=1))]
        Pxx_den_n = Pxx_den[np.where((f>=0.17) & (f<=1))]

        x = Pxx_den_n
        peaks, _ = find_peaks(x, height=0)

        # find max frequency
        index_max = np.argmax(x)
        freq_ori = f_n[index_max]
        #freq = "{:.3f}".format(freq_ori)
        #rr = "{:.1f}".format(freq_ori*60)
        freq = freq_ori
        rr   = freq*60

        return rr, freq

###############################################################################