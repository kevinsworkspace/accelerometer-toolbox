class data:
###############################################################################
    # function to load Accelerometer data to Pandas DataFrame format
    def load(file,type):
        import pandas as pd
        import numpy as np
        
        if type=='accel' or type=='ecg':
            df_ori = pd.read_csv(file)
            # convert Time to index as DataFrame format
            df_ori.index = pd.to_datetime(df_ori['Time'], unit='ms', utc=False)
            # remove the column for Unix timestamp 
            df_ori = df_ori.drop(columns=['Time'])

        elif type=='rr':
            df_ori = pd.read_csv(file)
            # convert Time to index as DataFrame format
            df_ori.index = pd.to_datetime(df_ori['timestamp'], unit='s', utc=False)
            # remove the column for Unix timestamp 
            df_ori = df_ori.drop(columns=['timestamp']) 
            df_ori.index.name = 'Time'
            df_1s = []          
        
        if type=='accel':
            # calculate SMV (Signal Magnitude Vector)
            df_ori['SMV']=np.sqrt(df_ori.accel_x**2+df_ori.accel_y**2+df_ori.accel_z**2)

        if type=='accel' or type=='ecg':
            # down-sample to 1-sec
            df_1s = df_ori.resample('1S').median() # for 1-sec resolution 
            # df_1m = df_ori.resample('1T').median() # for 1-min resolution 
        
        return df_ori, df_1s 

###############################################################################
    def cut(df, start, end, type):
        from datetime import datetime
        import pandas as pd
        
        start_time = datetime.strptime( start, '%Y-%m-%d %H:%M:%S' )
        end_time   = datetime.strptime(   end, '%Y-%m-%d %H:%M:%S' )
        df_cut = df[(df.index >= start_time) & (df.index <= end_time)]

        if type=='accel':
            output = pd.concat([df_cut.accel_x, df_cut.accel_y, df_cut.accel_z, df_cut.SMV], axis=1, sort=False) 
        elif type=='ecg':
            output = pd.concat([df_cut.ec], axis=1, sort=False)

        return output


    def cut2(df, start_time, end_time):
        from datetime import datetime
        import pandas as pd
        
        df_cut = df[(df.index >= start_time) & (df.index <= end_time)]
        output = pd.concat([df_cut.accel_x, df_cut.accel_y, df_cut.accel_z, df_cut.SMV], axis=1, sort=False) 

        return output        
###############################################################################     